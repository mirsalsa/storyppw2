from django.shortcuts import render
from django.http import JsonResponse
import requests
import json


# Create your views here.
def books(request):
    response = {}
    return render(request,'books.html',response)

def fungsi_data(request):
    url= "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)
    print(ret.content)

    data = json.loads(ret.content)
    # data={'coba' :'ini'}
    return JsonResponse(data, safe=False)

