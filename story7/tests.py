from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .apps import Story7Config
from .views import profile


# Create your tests here.
class TestStory7(TestCase):
    def test_url_exists(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_func_exists(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profile)

    def test_templ_exists(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profile.html')

    def test_apps(self):
        self.assertEqual(Story7Config.name, 'story7')
        self.assertEqual(apps.get_app_config('story7').name, 'story7')
