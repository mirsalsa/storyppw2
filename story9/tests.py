from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.apps import apps
from .apps import Story9Config
from .views import signupPage, loginPage, logoutPage, story9
from .forms import CreateUserForm


# Create your tests here.
class TestStory9(TestCase):
    def test_url_login_exists(self):
        response = Client().get('/story9/login/')
        self.assertEqual(response.status_code, 200)

    def test_url_signup_exists(self):
        response = Client().get('/story9/signup/')
        self.assertEqual(response.status_code, 200)

    def test_func_login_exists(self):
        found = resolve('/story9/login/')
        self.assertEqual(found.func, loginPage)

    def test_func_signup_exists(self):
        found = resolve('/story9/signup/')
        self.assertEqual(found.func, signupPage)

    def test_func_exists(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, story9)

    def test_template_login_exists(self):
        response = Client().get('/story9/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_template_signup_exists(self):
        response = Client().get('/story9/signup/')
        self.assertTemplateUsed(response, 'signup.html')

    def test_apps(self):
        self.assertEqual(Story9Config.name, 'story9')
        self.assertEqual(apps.get_app_config('story9').name, 'story9')

    def test_user_form_is_valid(self):
        user = CreateUserForm(data={'username':"akuaku", 'email':"aku@aku.com", 'password1':"buahalpukat", 'password2':"buahalpukat"})
        self.assertTrue(user.is_valid())

    def test_user_form_is_not_valid(self):
        user = CreateUserForm(data={})
        self.assertFalse(user.is_valid())

    def setUp(self):
        self.login = reverse("story9:login")
        self.signup = reverse("story9:signup")

    def test_login_view(self):
        response = self.client.get(self.login)
        self.assertEqual(response.status_code, 200)

    def test_signup_view(self):
        response = self.client.get(self.signup)
        self.assertEqual(response.status_code, 200)

    def testLogin(self):
        user = CreateUserForm(data={'username':"akuaku", 'email':"aku@aku.com", 'password1':"buahalpukat", 'password2':"buahalpukat"})
        self.client.login(username='john', password='johnpassword')
        response = self.client.get(self.login)
        self.assertEqual(response.status_code, 200)



