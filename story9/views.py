
from django.shortcuts import render, redirect 
from django.http import HttpResponse
from django.forms import inlineformset_factory
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import CreateUserForm

# Create your views here.
#from .models import *

def signupPage(request):
	if request.user.is_authenticated:
		return redirect('/story9/')
	else:
		form = CreateUserForm()
		if request.method == 'POST':
			form = CreateUserForm(request.POST)
			if form.is_valid():
				form.save()
				user = form.cleaned_data.get('username')

				return redirect('/story9/')
			

		context = {'form':form}
		return render(request, 'signup.html', context)

def loginPage(request):
	if request.user.is_authenticated:
		return redirect('/story9/')
	else:
		if request.method == 'POST':
			username = request.POST.get('username')
			password =request.POST.get('password')

			user = authenticate(request, username=username, password=password)

			if user is not None:
				login(request, user)
				return redirect('/story9/')
			else:
				messages.info(request, 'Username OR password is incorrect')

		context = {}
		return render(request, 'login.html', context)

def logoutPage(request):
	logout(request)
	return redirect('/story9/login')

@login_required(login_url='/story9/login')
def story9(request):
    return render(request, 'story9.html')
