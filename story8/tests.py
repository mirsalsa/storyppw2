from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.apps import apps
from .apps import Story8Config
from .views import books, fungsi_data


# Create your tests here.
class TestStory8(TestCase):
    def test_url_exists(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)

    def test_func_exists(self):
        found = resolve('/books/')
        self.assertEqual(found.func, books)

    # def test_create_account(self):
    #     """
    #     Ensure we can create a new account object.
    #     """
    #     url = reverse('fungsi_data')
    #     response = Client.get(url, format='json')
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(len(response.data), 1)
    
    # def test_ok(self):
    #     assert self.client.get('/data/?q=frozen').json()['modified']

    def test_templ_exists(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'books.html')

    def test_apps(self):
        self.assertEqual(Story8Config.name, 'story8')
        self.assertEqual(apps.get_app_config('story8').name, 'story8')
