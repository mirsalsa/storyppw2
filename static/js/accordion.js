$(document).ready(function() {
  
    $("a.bar").click(function() {
  
      if ($(event.target).is('button')) {
        event.preventDefault();
        return;
      }
  
      if ($(this).hasClass("active")) {
        $(this).removeClass("active");
        $(this)
          .siblings(".content")
          .slideUp(500);
      } else {
        $("a.bar").removeClass("active");
        $(this).addClass("active");
        $(".content").slideUp(500);
        $(this)
          .siblings(".content")
          .slideDown(500);
      }
  
    });
  
    $('.up').click(function() {
        var item = $(this).parents('div.set'),
            swapWith = item.prev();
        item.after(swapWith);
    });
  
    $('.down').click(function() {
        var item = $(this).parents('div.set'),
            swapWith = item.next();
        item.before(swapWith);
    });

  });
  